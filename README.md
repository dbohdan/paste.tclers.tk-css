# paste.tclers.tk CSS

## Building on Debian/Ubuntu

```sh
sudo apt install make minify tcl
make
```

The end result of the build is the file `combined.css`.

To use the new style sheet you will need to add the following tag to the
`<head>` of your documents.

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

You should also remove this line:

```html
<script type="text/javascript" src="/stripe.js"></script>
```

## License

MIT.  See [LICENSE](LICENSE) and [AUTHORS](AUTHORS).

[normalize.css](https://github.com/necolas/normalize.css) is copyright Nicolas
Gallagher and Jonathan Neal and distributed under the MIT license.  See
[vendor/normalize.css/LICENSE.md](vendor/normalize.css/LICENSE.md).
