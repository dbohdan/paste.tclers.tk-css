all: combined.css

combined.css: combined.full.css
	minify -o $@ $<

combined.full.css: layout.css vendor/normalize.css/normalize.css
	cat vendor/normalize.css/normalize.css > $@
	cat $< >> $@

layout.css: css.tcl
	tclsh $< > $@

.PHONY: all
