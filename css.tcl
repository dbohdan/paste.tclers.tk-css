#! /usr/bin/env tclsh
# This script generates layout.css.
# Copyright (c) 2020-2021, 2024 D. Bohdan.
# License: MIT.

package require Tcl 8.6 9

proc gen {} {
    set bg #fff
    set text #000
    set main #f7e7ba
    set mainLight $bg
    set gray #eee

    set tclComment #d00
    set tclKeyword #00d
    set tclVariable #0a0
    set tclString #084

    set mobileWidth 65
    set onDesktop "@media (min-width: ${mobileWidth}rem)"
    set onMobile "@media (max-width: [expr {$mobileWidth - 0.001}]rem)"

    unindent [subst -nobackslashes -nocommands {
        /*! paste.tclers.tk CSS | MIT License |\
            gitlab.com/dbohdan/paste.tclers.tk-css */

        html {
            background: $mainLight;
            box-sizing: border-box;
            font-size: 15px;
        }

        a:not(:hover) {
            text-decoration: none;
        }

        body {
            background: $bg;
            font-family: Arial, Helvetica, sans-serif;
            max-width: 82rem;
            margin: 0 auto;
        }

        #header > h1 {
            font-size: 1.5rem;
            text-align: center;
            margin-top: 0;
            padding-top: 1rem;
        }

        #body {
            display: flex;
            padding: 0 1rem 0 1rem;
        }

        /* Hide empty divs. */
        #body > #clear, #body > #footer {
            display: none;
        }

        p.last {
            height: 1rem;
            margin: 0;
        }

        h1, h2, h3, h4, h5, h6 {
            font-size: 120%;
        }

        #body h1, #content h3, #comments h2, #comments h3, #sidebar h1 {
            background: $main;
            margin: 0;
            padding: 0.2rem 1rem 0.2rem 1rem;
        }
        #sidebar h1:not(:first-of-type), #comments h3 {
            border-top: 0.75rem solid $bg;
        }

        .h3r {
            display: inline-block;
            text-align: right;
            padding-left: 1rem;
        }

        #recent {
            width: 100%;
            word-break: break-word;
        }

        /* Alternate table row colors. */
        #recent tr:nth-child(even) td { background: $bg; }
        #recent tr:nth-child(odd) td { background: $gray; }

        /* The "user" column. */
        #recent > tbody > tr > th:nth-child(2),
        #recent > tbody > tr > td:nth-child(2)
        {
            min-width: 4rem;
        }
        $onMobile {
            /* Hide the "chan" column.  Nobody uses channels other than
               "tcl". */
            #recent > tbody > tr > th:nth-child(1),
            #recent > tbody > tr > td:nth-child(1)
            {
                display: none;
            }

            /* "Comments". */
            #recent > tbody > tr > th:nth-child(4) { font-size: 50%; }
            #recent > tbody > tr > th:nth-child(4),
            #recent > tbody > tr > td:nth-child(4)
            {
                max-width: 1rem;
            }

            /* "Date". */
            #recent > tbody > tr > th:nth-child(5),
            #recent > tbody > tr > td:nth-child(5)
            {
                min-width: 2rem;
            }
        }

        /* Comments. */
        #comments > form, .comment > * {
            margin-left: 1.5rem;
        }

        #c_comment {
            width: 90%;
            margin-bottom: 0.5rem;
        }

        /* New paste form. */
        #new label {
            display: inline-block;
            width: 6rem;
            font-weight: bold;
            margin-top: 0.75rem;
            margin-right: 0.5rem;
        }

        #new label[for="new_p"] {
            margin-top: 1.25rem;
            margin-bottom: 0.25rem;
        }

        #new_p {
            width: 90%;
        }

        #sidebar {
            background: $gray;
            padding-bottom: 0.1rem;
        }

        $onDesktop {
            #content {
                flex-grow: 1;
                flex-shrink: 1;
                flex-base: 52rem;
                max-width: 68rem;
            }

            #sidebar {
                border-left: 1rem solid $bg;
                flex-base: 10rem;
                padding-bottom: 0;
            }
        }
        $onMobile {
            #body {
                flex-direction: column;
            }

            /* Break up the page list. */
            #content > a:nth-child(7):after,
            #content > a:nth-child(12):after,
            #content > a:nth-child(17):after,
            #content > a:nth-child(22):after
            {
                content: "\a";
            }
        }

        .code {
            font-family: monospace;
            font-size: 1.0rem;
            white-space: pre-wrap;
            margin-left: 1rem;
            margin-right: 1rem;
        }
        .code li:nth-child(even) { background: $bg; }
        .code li:nth-child(odd) { background: $gray; }
        $onMobile {
            .code {
                font-size: 1rem;
                margin-left: 0.5rem;
                margin-right: 0.35rem;
            }
        }

        /* Tcl syntax. */
        .tcl_cmt { color: $tclComment; }
        .tcl_key { color: $tclKeyword; }
        .tcl_var { color: $tclVariable; }
        .tcl_str { color: $tclString; }
    }]
}

proc unindent {
    text
    {chars { }}
    {ignoreIndentOnlyLines true}
    {max inf}
} {
    regsub ^\n $text {} text
    if {$ignoreIndentOnlyLines} {
        regsub \n\[$chars\]*?$ $text {} text
    } else {
        regsub \n$ $text {} text
    }

    set rLeading ^\[$chars\]*
    set rBlankLine $rLeading$

    foreach line [split $text \n] {
        if {$line eq {}
            || ($ignoreIndentOnlyLines
                && [regexp $rBlankLine $line])} continue

        regexp -indices $rLeading $line idc
        set count [expr {[lindex $idc 1] + 1}]

        set max [expr {min($max,$count)}]
    }

    set start [expr { $max == inf ? {end+1} : $max }]

    join [lmap line [split $text \n] {
        string range $line $start end
    }] \n
}

# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    fconfigure stdout -encoding utf-8
    puts [gen]
}
